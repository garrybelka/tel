# stream - schema

# schema - dkid, {field_name: DType}

class DType:
    Int = 'i'
    Long = 'l'
    Float = 'f'
    Text = 't'
