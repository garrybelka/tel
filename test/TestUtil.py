import unittest
from util import *

class ConfigTestCase(unittest.TestCase):
    def test_config(self):
        config = Config('tel.cfg', {})
        cfg = config.cfg
        self.assertEqual(len(cfg), 2)
        self.assertEqual(cfg,
                         {"cassandra.contact_points": ["127.0.0.1"], "cassandra.keyspace": "tel" })

    def test_default_settings(self):
        config = Config('tel.cfg', {'defA':1, 'defB':2, "cassandra.keyspace": 3})
        cfg = config.cfg
        self.assertEqual(len(cfg), 4)
        self.assertEqual(cfg,
                         {'defA':1, 'defB':2, "cassandra.contact_points": ["127.0.0.1"], "cassandra.keyspace": "tel" })

class GenNameTestCase(unittest.TestCase):
    def test_gn(self):
        gn = GenName()
        self.assertEqual(gn.gen('a'), 'a1')
        self.assertEqual(gn.gen('a'), 'a2')
        g2 = GenName()
        self.assertEqual(gn, g2)
        self.assertEqual(gn.gen('b'), 'b1')
        self.assertEqual(g2.gen('a'), 'a3')


if __name__ == '__main__':
    unittest.main()
