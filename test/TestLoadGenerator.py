import unittest

from loademul import LoadGenerator
from loademul import Event
from loademul import RunExecutor


class LoadGeneratorTestCase(unittest.TestCase):
    def setUp(self):
        self.lg = LoadGenerator(None, {})

    def spec_run(self, spec_gen, spec, n_runs):
        tg = spec_gen(spec)
        r = []
        for i, t in enumerate(tg):
            if i >= n_runs:
                break
            r.append(t)
        return r

    def tspec_run(self, tspec, n_runs):
        return self.spec_run(self.lg.tspec_gen, tspec, n_runs)

    def ipspec_run(self, ipspec, n_runs):
        return self.spec_run(self.lg.ip_spec_gen, ipspec, n_runs)


    def test_tspec(self):
        # const
        self.assertListEqual(self.tspec_run(5, 3), [5,5,5])

        # random
        self.assertListEqual(self.tspec_run((5, 6), 3), [5,5,5])
        # more random
        for n in 2, 5, 8, 25:
            r = self.tspec_run((5, 10), n)
            self.assertEqual(len(r), n)
            for t in r:
                self.assertIn(t, set(range(5, 10)))

        # regular
        self.assertListEqual(self.tspec_run((5, 6, 1), 3), [5,5,5])
        self.assertListEqual(self.tspec_run((5, 8, 2), 4), [5,7,6,5])


    def test_ipspec(self):
        self.assertListEqual(self.ipspec_run('1.2.3.4', 3), ['1.2.3.4']*3)
        self.assertListEqual(self.ipspec_run(('1.2.3.4',), 3), ['1.2.3.4', '1.2.3.5', '1.2.3.6'])
        for n in 2, 5, 8, 1000:
            r = self.ipspec_run(('1.2.3.0',8), n)
            self.assertEqual(len(r), n)
            for s in r:
                self.assertEqual(s[:6], '1.2.3.')
                self.assertTrue(0 <= int(s[6:]) <= 255)

    def test_session1(self):
        c = {
            'cmd': 'client',
            'count': 1,
            'start': 1000,
            'duration': 10000,
            'bandwidth': 1000000,
            'bitrate': 1000000,
            'request_play_at_start': True,
        }
        self.rex = RunExecutor()
        self.lg = LoadGenerator(self.rex, [c])
        self.lg.run()
        self.rex.run()
        self.assertEqual(len({m['sid'] for m in self.lg.received}), 1)


    def test_session100(self):
        c = {
            'cmd': 'client',
            'count': 100,
            'start': (1000, 11000, 200),
            'duration': 10000,
            'bandwidth': 1000000,
            'bitrate': 1000000,
            'request_play_at_start': True,
        }
        self.rex = RunExecutor()
        self.lg = LoadGenerator(self.rex, [c])
        self.lg.run()
        self.rex.run()
        print [m['events'] for m in self.lg.received]
        self.assertEqual(len({m['sid'] for m in self.lg.received}), 100)
        self.assertEqual(min(m['ts'] for m in self.lg.received), 1000)
        self.assertEqual(max(m['ts'] for m in self.lg.received), 20800)
        self.assertEqual(sum(1 for m in self.lg.received for e in m['events'] if e['event'] == Event.LOADING), 100)
        self.assertEqual(sum(1 for m in self.lg.received for e in m['events'] if e['event'] == Event.PLAYING), 100)

if __name__ == '__main__':
    unittest.main()
