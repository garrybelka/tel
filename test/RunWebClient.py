import json
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from twisted.python import log

from loademul import WebClient

def loopn(n, f, t):
    reactor.callLater(t, f)
    if n > 0:
        reactor.callLater(t, lambda : loopn(n - 1, f, t))


def main():
    import sys
    log.startLogging(sys.stdout)
    w = WebClient('http://127.0.0.1:5555')
    loopn(5, lambda : w.send({'test':'sent'}), 1)
    reactor.callLater(7, lambda : reactor.stop())
    reactor.run()


if __name__ == "__main__":
    main()