import json
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from twisted.python import log


# This is just about the simplest possible protocol
class Server(Protocol):
    def dataReceived(self, data):
        self.transport.write(json.dumps({'resp':'ok'}))
        log.msg('%s' % data)
        print 'received %s' % data


def main():
    import sys
    log.startLogging(sys.stdout)

    f = Factory()
    f.protocol = Server
    reactor.listenTCP(5555, f)

    reactor.run()
    #reactor.callLater(7, lambda : reactor.stop())


if __name__ == "__main__":
    main()