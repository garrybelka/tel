import unittest

from loademul import VideoPlayerEmulator
from loademul import Event
from loademul import RunExecutor


class VpeTestCase(unittest.TestCase):
    def project(self, dlist, list):
        return [[d[x] for x in list] for d in dlist]

    def test_loading(self):
        cfg = {'bandwidth': 1000000, 'bitrate': 2000000, 'end_ms': 100000}
        rex = RunExecutor()
        self.received = []
        vpe = VideoPlayerEmulator(rex, cfg, self)
        vpe.run()
        rex.run()
        self.assertEqual(len(self.received), 8)
        events = [e for r in self.received for e in r['events']]
        self.assertEqual(self.project(events, ['event', 'ts', 'total_loaded']),
                         [[Event.LOADING, 0, 0],
                          [Event.LOADING_OFF, 40000, 5000000]])

    def test_playing_same_rate(self):
        cfg = {'bandwidth': 1000000, 'bitrate': 1000000, 'end_ms': 100000, 'request_play_at_start': True}
        rex = RunExecutor()
        self.received = []
        vpe = VideoPlayerEmulator(rex, cfg, self)
        vpe.run()
        rex.run()
        #util.dlist_pprint(self.received)
        events = [e for r in self.received for e in r['events']]
        self.assertEqual(self.project(events, ['event', 'ts']),
                         [[Event.LOADING, 0],
                          [Event.PLAYING, 2000]])

    def test_playing(self):
        cfg = {'bandwidth': 1000000, 'bitrate': 2000000, 'end_ms': 100000}
        rex = RunExecutor()
        self.received = []
        vpe = VideoPlayerEmulator(rex, cfg, self)
        def mk(e):
            def f():
                vpe.simulate_event(e)
            return f
        for e, ts in [(Event.PLAY, 10000),
                      (Event.PAUSE, 25000),
                      (Event.PLAY, 50000),
                      (Event.STOP, 80000),
                     ]:
            rex.submit(mk(e), at=ts)
        vpe.run()
        rex.run()
        #util.dlist_pprint(self.received)
        events = [e for r in self.received for e in r['events']]
        self.assertEqual(self.project(events, ['event', 'ts']),
                         [[Event.LOADING, 0],
                          [Event.PLAY, 10000],
                          [Event.PLAYING, 10000],
                          [Event.PLAYING_OFF, 20000],
                          [Event.PLAYING, 24000],
                          [Event.PAUSE, 25000],
                          [Event.PLAYING_OFF, 25000],
                          [Event.PLAY, 50000],
                          [Event.PLAYING, 50000],
                          [Event.PLAYING_OFF, 78000],
                          [Event.STOP, 80000]])


    def send(self, m, vpe):
        self.received.append(m)
        r = {'sid': 1}
        vpe.receive_callback(r)
        return {'sid': 1}



if __name__ == '__main__':
    unittest.main()
