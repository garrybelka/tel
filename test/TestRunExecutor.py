import unittest
import sys

import mock

from loademul import RunExecutor


def get_m_args(m):
    return [a[0] for a in m.call_args_list]

class RunExecutorTestCase(unittest.TestCase):
    def test_1(self):
        trm = RunExecutor()
        li = (1, 5, 3, 8)
        for i in li:
            trm.submit(i, i)
        ls = sorted(li)
        res = [trm.next() for i in range(len(li))]
        self.assertEqual(res, [[i, i] for i in ls])

    def test_run(self):
        self.rex = RunExecutor()
        self.m = mock.Mock()
        self.rex.submit(lambda : self.callme(delay=1), 1)
        self.rex.submit(lambda : self.callme(at=4), 3)
        self.rex.submit(lambda : self.callme(), at=5)
        self.rex.run()
        self.assertTrue(self.m.call_args_list[0][0] == (1,))
        self.assertTrue(get_m_args(self.m) == [(1,), (2,), (3,), (4,), (5,)])

    def callme(self, delay=None, at=None, count=None):
        self.m(self.rex.cur_time_ms)
        if delay is not None:
            self.rex.submit(lambda : self.callme(), delay=delay)
        elif at is not None:
            self.rex.submit(lambda : self.callme(), at=at)
        elif count > 0:
            self.rex.submit(lambda : self.callme(count=count-1), at=self.rex.cur_time_ms)

    def test_0runs(self):
        self.rex = RunExecutor()
        self.m = mock.Mock()
        self.rex.submit(lambda : self.callme(count=4), 1)
        self.rex.run()
        print sys._getframe().f_code.co_name, self.m.call_args_list, get_m_args(self.m)
        self.assertTrue(self.m.call_args_list[0][0] == (1,))
        self.assertTrue(get_m_args(self.m) == [(1,), (1,), (1,), (1,), (1,)])


if __name__ == '__main__':
    unittest.main()
