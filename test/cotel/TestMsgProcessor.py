import yaml
import unittest
from MsgProcessor import MsgStruct, Field
from cotel import MsgProcessor


class MyTestCase(unittest.TestCase):
    def once(self, cfg, expected):
        mp = MsgProcessor(yaml.load(cfg))

        self.assertEqual(len(mp.name_msg_map), len(expected))
        for name in expected:
            self.assertEqual(mp.name_msg_map[name], expected[name])

    def test_parse_msg_cfg(self):
        self.once(""" {"prog": [
                    {"c": "msg", "n": "name1",
                     "fields": [{"n": "f1", "ty": "i"}]}
                  ]} """,
                  {'name1': MsgStruct(name="name1", fmap={"f1": Field("f1", ("i",), False, 0)})})
        self.once("""{"prog": [
                    {"c": "msg", "n": "name1",
                     "fields": [{"n": "f1", "ty": "i"}]},
                    {"c": "msg", "n": "name2", "base": "name1",
                     "fields": [{"n": "f2", "ty": "f", "opt":True},
                                {"n": "f4", "ty": "t", "maxlen": 20}]}
                  ]}""",
                  {'name1': MsgStruct(name="name1", fmap={"f1": Field("f1", ("i",), False, 0)}),
                   'name2': MsgStruct(name="name2", fmap={"f1": Field("f1", ("i",), False, 0),
                                                          "f2": Field("f2", ("f",), True, 0),
                                                          "f4": Field("f4", ("t",), False, 20)
                                                         })})
        self.once(""" {"prog": [
                    {"c": "msg", "n": "name1",
                     "fields": [{"n": "f1", "ty": "i"}]}
                  ]} """,
                  {'name1': MsgStruct(name="name1", fmap={"f1": Field("f1", ("i",), False, 0)})})



if __name__ == '__main__':
    unittest.main()
