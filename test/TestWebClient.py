import json
from twisted.trial import unittest
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor

from loademul import WebClient

### Protocol Implementation

# This is just about the simplest possible protocol
class Server(Protocol):
    def dataReceived(self, data):
        self.transport.write(json.loads({'resp':'ok'}))
        print data


class MyTestCase(unittest.TestCase):

    def test_something(self):

        f = Factory()
        f.protocol = Server
        reactor.listenTCP(5555, f)

        w = WebClient('http://127.0.0.1:5555')

        reactor.run()

        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
