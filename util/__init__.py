import sys
from Config import Config


class TODO_Exc(Exception):
    pass


def TODO(s='TODO failed'):
    raise TODO_Exc(s)


def ipOfInt(x):
    """ @param x int as ip address
        @return string representation of ip address in dot notation
    """
    return '.'.join(str((x >> (i * 8))&0xff) for i in range(3, -1, -1))


def ipOfStr(s):
    """ @param s a string with ip in dot notation
        @return int
    """
    x = 0
    for b in s.split('.'):
        x = (x << 8) + int(b)
    return x


def dlist_pprint(dlist):
    """ print nicely collection of dicts """
    for d in dlist:
        print '  {'
        for k in sorted(d.iterkeys()):
            print '    %s: %s' % (k, d[k])
        print '  }'


class Context(object):
    def __init__(self):
        self.config = None
        self.log = None

    def set_log(self, path=None, kind='file'):
        if kind == 'file':
            self.log = open(path, 'w') if path is not None else sys.stdout
            return
        raise ValueError('Unsupported log kind %s' % kind)

    def set_config(self, config):
        self.config = config


# defining singleton in Python 2:
#   class MyClass(BaseClass):
#       __metaclass__ = Singleton
# and in Python 3:
#   class MyClass(BaseClass, metaclass=Singleton):
#       pass

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class GenName(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.prefix_count_map = {}

    def gen(self, prefix):
        try:
            n = self.prefix_count_map[prefix] + 1
        except KeyError:
            n = 1
        self.prefix_count_map[prefix] = n
        return prefix + str(n)
