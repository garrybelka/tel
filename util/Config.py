import json
import logging


LOG = logging.getLogger(__name__)


class Config(object):
    """ self.cfg is a dict with cfg
    """
    def __init__(self, cfgPath, defaultSettings):
        LOG.info('Config from %s', cfgPath)
        self.cfgPath = cfgPath
        self.cfg = self.load_cfg()
        #self.validate_cfg()
        for k, v in defaultSettings.iteritems():
            if k not in self.cfg:
                self.cfg[k] = v

    def load_cfg(self):
        """ load and parse cfg file
            @return dict with cfg
            throws IOError if load failed
        """
        with open(self.cfgPath) as f:
            cfg = json.load(f)
            return cfg

    def update_cfg(self):
        old = self.cfg
        self.cfg = self.load_cfg()
        # TODO produce {'new': list, 'modified': list, 'deleted': list}