from collections import namedtuple
from twisted.python import log
from util import TODO, GenName

# {prog: [
#   {c: 'msg', n: Name, +base: MsgName, +is: MsgCond,
#    fields: [
#       {n: Name, ty: Type, +opt: True|False, +maxlen: Len} ] }
#   {c: 'ingest', n: Name, content: 'json'|'thrift', transport: 'http'|'tcp', +url: "..."}
#   {c: 'ply', n: Name, source: Name|[Name,], ps: [PCmd|Explode, ]}
#   {c: 'emit', n: Name, content: 'json'|'thrift', transport: http|tel|hdfs,
# ]}

# MsgCond = "python code" |
#           {field: value|{set of values}|{RelOp: value,}|(range_start, range_end), }
# AType = FType | 'm' MsgName
# Type = AType | ('list'|'set' AType)| ('map' AType AType)
# PCmd = {c: 'issue', +cond: MsgCond, +ty:MsgName, to:Name, res:[BuildCmd]}
# Explode = {c: 'explode', +cond: MsgCond, field:FieldName, cmds: [PCmd]}
# BuildCmd = {c: 'copy', all:True | fields:[FldName, ], +outer:True}|
#            {c: '+', n:Name, +ty:Type, (f:"python code" | field:FieldName +outer:True) }|
#            {c: '-', n:Name}
# Field type designator


class FType:
    Int = 'i'
    Long = 'l'
    Float = 'f'
    Text = 't'
    Bool = 'b'
    Msg = 'm'
    SIMPLE = {Int, Long, Float, Text, Bool}
    ALL = SIMPLE | {Msg}


MsgStruct = namedtuple('MsgStruct', 'name fmap ispred')
Field = namedtuple('Field', 'name ty opt maxlen')


class MsgProcessor(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.parse_prog()
        self.gen_name_helper = GenName()

    def gen_name(self, prefix):
        return self.gen_name_helper.gen(prefix)

    def parse_prog(self):
        p = self.cfg['prog']
        self.name_msg_map = {}
        self['prog'] = []
        for c in p:
            if c['c'] == 'msg':
                m = self.parse_msg_cfg(c)
                self.name_msg_map[m.name] = m
            elif c['c'] == 'ingest':
                r = self.parse_ingest(c)
            elif c['c'] == 'emit':
                r = self.parse_emit(c)
            elif c['c'] == 'ply':
                r = self.parse_ply(c)
            # TODO
            self['prog'].append(r)
        # TODO check cross refs, collect code

    def parse_msg_cfg(self, c):
        name = c['n']
        if name in self.name_msg_map:
            raise AttributeError("redefining msg '%s' cfg" % name)
        fmap={}
        base = c.get('base', None)
        if base:
            try:
                fmap = self.name_msg_map[base].fmap.copy()
            except KeyError:
                raise AttributeError("msg '%s' cfg refers to undefined base '%s'" % (name, base))

        for cf in c.get('fields', []):
            try:
                f = self.parse_field_cfg(cf)
            except AttributeError as e:
                raise AttributeError("cfg msg '%s' %s" % (name, e.message))
            if f in fmap:
                raise AttributeError("msg '%s' cfg redefining field '%s'" % (name, f.name))
            fmap[f.name] = f
        ispred = self.parse_msg_cond(c.get('is', None))
        return MsgStruct(name=name, fmap=fmap, ispred=ispred)

    def parse_field_cfg(self, cf):
        def check_ty(t):
            if t in FType.SIMPLE:
                return (t,)
            elif isinstance(t, (tuple, list)) and len(t) == 2 and t[0] == FType.Msg:
                m = t[1]
                if m not in self.name_msg_map:
                    raise AttributeError("unknown or incorrect msg spec in field spec %s" % cf)
                return (FType.Msg, m)
            return None

        def get_attr(k):
            try:
                return cf[k]
            except KeyError:
                raise AttributeError("missing '%s' in field spec %s" % (k, cf))

        name = get_attr('n')
        t = get_attr('ty')
        try:
            ty = check_ty(t)
            if ty is None and isinstance(t, (tuple, list)):
                if t[0] in ('list', 'set'):
                    s = check_ty(t[1:])
                    if s is not None:
                        ty = (t[0],) + s
                elif t[0] == 'map':
                    key = check_ty(t[1])
                    v = check_ty(t[2:])
                    if key is not None and v is not None:
                        ty = ('map', key) + v
            if ty is None:
                raise AttributeError("incorrect field spec %s" % cf)

            return Field(name=name,
                      ty=ty,
                      opt=cf.get('opt', False),
                      maxlen=cf.get('maxlen', 0))
        except KeyError:
            raise AttributeError("incorrect field spec %s" % cf)

    # MsgCond = "python code(m)" | {field: value|{set of values}|{RelOp: value}|(range_start, range_end)...}
    # @return string "lambda m: cond"
    def parse_msg_cond(self, mc):
        if mc is None:
            return (lambda m: None)
        if isinstance(mc, str):
            return mc

        def field(n, v):
            if isinstance(v, (list, tuple) and len(v) == 2):
                return "%s <= m['%s'] < %s" % (v[0], n, v[1])
            elif isinstance(v, (dict) and len(v) == 1):
                op, val = list(v.iteritems())[0]
                return "m['%s'] %s %s" % (n, op, val)
            elif isinstance(v, (set)):
                return "%s in {%s}" % (n, ','.join(v))
            else:
                return "m['%s'] == %s" % (n, v)

        s = 'lambda m: ' + ' and '.join(field(n, v) for n, v in mc)
        return s

    #   {c: 'ply', n: Name, source: Name|[Name,], ps: [PCmd, ]}
    def parse_ply(self, pc):
        p = {}
        p['name'] = pc['n']
        p['source'] = list(pc['source'])
        p['plies'] = []
        for icmd in pc['ps']:
            if icmd['c'] == 'issue':
                r = self.parse_issue(icmd)
                p['plies'].append(r)
            elif icmd['c'] == 'explode':
                r = self.parse_explode(icmd)
                p['plies'].append(r)
            else:
                raise AttributeError("unknown ply ps command %s" % icmd)
        return p

    # PCmd = {c: 'issue', +cond: MsgCond, +ty:MsgName, to:Name, res:[BuildCmd,]}
    # BuildCmd = {c: 'copy', +fields:[FieldName, ], +outer:True}|
    #            {c: '+', n:Name, +ty:Type, (f:"python code" | field:FieldName +outer:True) }|
    #            {c: '-', n:Name, +outer:True}
    def parse_issue(self, ic, withOuter=False):
        r = {}
        r['cond'] = self.parse_msg_cond(ic.get('cond', None))
        r['ty'] = ic.get('ty', None)
        if r['ty'] and r['ty'] not in self.name_msg_map:
            raise AttributeError("unknown msg type %s in %s" % (r['ty'], ic))
        r['to'] = ic['to']
        r['code_name'] = self.gen_name('bc')
        code = ['def %s(m %s):' % (r['buildName'], (', om' if withOuter else '')),
                '  r = {}']
        for b in ic['res']:
            outer = b.get('outer', False)
            if outer and not withOuter:
                raise AttributeError("outer is requested (True) not in outer context: %s" % ic)
            operand = 'om' if outer else 'm'
            if b['c'] == 'copy':
                if 'fields' in b:
                    code += ['  fields = ["%s"]' % '","'.join(b['fields']),
                             '  for f in fields:' ,
                             '    try:' ,
                             '      r[f] = %s[f]' % operand,
                             '    except KeyError:',
                             '        pass'
                            ]
                else:
                    code += ['  for f, v in %s.iteritems():' % operand,
                             '       r[f] = v',
                            ]
            elif b['c'] == '+':
                if 'f' in b:
                    code += ['  f = %s' % b['f'] ]
                    if outer:
                        code += ['  r["%s"] = f(m, om)' % b['n']]
                    else:
                        code += ['  r["%s"] = f(%s)' % (b['n'], operand)]
                elif 'field' in b:
                    code += ['  r["%s"] = %s["%s"]' % (b['n'], operand, b['field'])]
                else:
                    raise AttributeError("build cmd '+' incorrect: no f or field defined: %s" % b)
            elif b['c'] == '-':
                code += ['  del %s["%s"]' % (operand, b['n'])]
        code += ['  return r', '']
        r['code'] = '\n'.join(code)
        return r

    # Explode = {c: 'explode', +cond: MsgCond, field:FieldName, cmds: [PCmd]}
    def parse_explode(self, ec):
        r = {}
        r['cond'] = self.parse_msg_cond(ec.get('cond', None))
        r['for'] = ec['field']
        r['cmds'] = [self.parse_issue(ic, withOuter=True) for ic in ec['cmds']]
        return r

    def submit(self, m):
        log.msg('MsgProcessor: %s' % m)

    def parse_ingest(self, c):
        pass    # TODO

    def parse_emit(self, c):
        pass    # TODO
