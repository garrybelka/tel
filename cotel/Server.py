import json
import sys
from twisted.python import log

from twisted.web.server import Site
from twisted.web.resource import Resource, NoResource, ErrorPage
from twisted.web._responses import BAD_REQUEST
from twisted.internet import reactor

import cgi
from util import Config
from cotel import MsgProcessor


class Page(Resource):
    OK_RESP = json.dumps({'resp':'ok'})
    isLeaf = True

    def __init__(self, process_msg):
        Resource.__init__(self)
        self.process_msg = process_msg

    def render_GET(self, request):
        return "<html><body><pre>%s</pre></body></html>" % NoResource()

    def render_POST(self, request):
        m = json.loads(cgi.escape(request.content.read()))
        try:
            self.process_msg(m)
            return self.OK_RESP
        except RuntimeError as e:
            return "<html><body><pre>%s</pre></body></html>" % ErrorPage(BAD_REQUEST, 'Bad Request',
                                                                         'Bad Request or processing error %s' % e)


class WebServer(object):
    def __init__(self, cfg, process_msg):
        self.root = Page(process_msg)
        factory = Site(self.root)
        reactor.listenTCP(cfg['in.port'], factory)


def main():
    config = Config(sys.argv[1], {})
    cfg = config.cfg
    log.startLogging(open(cfg['log']) if 'log' in cfg else sys.stdout)
    msg_processor = MsgProcessor(cfg)
    ws = WebServer(cfg, msg_processor.submit)
    reactor.run()

if __name__ == "__main__":
    main()
