import random
import uuid

import loademul
import util


# {cmd='client' count=n start=tspec duration=tspec events=[] errors=[] disconnect=(tspec, tspec) loss_ratio=x
#   ip=ipspec, bandwidth=, bitrate= user= settings={k:v,} }
# {'cmd': 'global', 'sid': sid, 'uuidsid': True, 'remote': url}

class LoadGenerator(object):
    def __init__(self, rex, cfglist, done_cb=None):
        self.rex = rex
        self.done_cb = done_cb
        self.received = []
        self.clients = []

        self.web_client = None
        default_global = {'cmd':'global', 'sid': 1}
        for c in [default_global] + cfglist:
            self.check_cfg(c)
            if c['cmd'] == 'client':
                self.clients += [p for p in self.make_clients(c)]
            elif c['cmd'] == 'global':
                if 'uuidsid' in c:
                    self.is_uuid_sid = c.get('uuidsid')
                if 'sid' in c:
                    self.sid_gen = self.sidspec_gen(c['sid'])
                    self.is_uuid_sid = False
                if 'remote' in c:
                    self.web_client = loademul.WebClient(c['remote'].encode('ascii'))

    def run(self):
        self.n_left = len(self.clients)
        for p in self.clients:
            p.run()

    def client_done_cb_handler(self):
        self.n_left -= 1
        if self.n_left <= 0:
            self.done_cb()

    def check_cfg(self, c):
        keywords = {'client': {'cmd', 'start', 'duration', 'bandwidth', 'count',
                               'bitrate', 'ip', 'settings', 'request_play_at_start'},
                    'global': {'cmd', 'sid', 'uuidsid', 'remote'}}
        try:
            kws = keywords[c['cmd']]
        except KeyError:
            raise KeyError('bad cfg: bad or missing cmd %s', c)
        bad = [k for k in c.iterkeys() if k not in kws]
        if bad:
            raise KeyError('bad cfg: bad attrs %s in %s', bad, c)

    def make_clients(self, c):
        n = c.get('count', 1)
        start_gen = self.tspec_gen(c.get('start', 0))
        duration_gen = self.tspec_gen(c.get('duration', 100000))
        bw_gen = self.tspec_gen(c.get('bandwidth', 1000000))
        br_gen = self.tspec_gen(c.get('bitrate', 1000000))
        ip_gen = self.ip_spec_gen(c.get('ip', '1.2.4.1'))
        for i in xrange(n):
            start = start_gen.next()
            duration = duration_gen.next()
            cfg = {'start_ms': start,
                   'end_ms': start + duration,
                   'bandwidth': bw_gen.next(),
                   'bitrate': br_gen.next(),
                   'ip': ip_gen.next(),
                   'settings': c.get('settings', {}),
                   'sid': self.sid_gen.next(),
                   'request_play_at_start': c.get('request_play_at_start', False),
            }
            yield loademul.VideoPlayerEmulator(self.rex, cfg, self, done_cb=self.client_done_cb_handler)

    def tspec_gen(self, tspec):
        """ @param tspec const generates this const infinitely
                         (low, upper) generates random values in that range
                         (k, n, step) generates k + (i * step) % (n - k) on i-th iteration
        """
        if isinstance(tspec, int):
            while True:
                yield tspec
            return
        if not isinstance(tspec, (list,tuple)):
            raise ValueError('incorrect tspec %s' % tspec)

        if len(tspec) == 2:
            # random
            low, upper = tspec
            while True:
                yield random.randrange(low, upper)
            return

        if len(tspec) == 3:
            # regular
            low, upper, step = tspec
            delta = upper - low
            cur = 0
            while True:
                yield low + cur
                cur = (cur + step) % delta

    def ip_spec_gen(self, ipSpec):
        """ @param ipSpec const str, e.g. '1.2.3.4', generates this const infinitely
                         (base,) generates sequential ip addresses starting from base
                         (base, nbits) generates random ips within base/nbits subnet
        """
        if isinstance(ipSpec, str):
            while True:
                yield ipSpec

        if not isinstance(ipSpec, (list,tuple)):
            raise ValueError('incorrect ipspec %s' % ipSpec)

        if len(ipSpec) == 1:
            ipBaseInt = util.ipOfStr(ipSpec[0])
            i = 0
            while True:
                x = ipBaseInt + i
                yield '.'.join(str((x >> (i * 8))&0xff) for i in range(3, -1, -1))
                i += 1

        if len(ipSpec) == 2:
            ipBaseStr, nLowBits = ipSpec
            ipBaseInt = util.ipOfStr(ipBaseStr)
            while True:
                x = ipBaseInt + random.getrandbits(nLowBits)
                yield '.'.join(str((x >> (i * 8))&0xff) for i in range(3, -1, -1))

        raise ValueError('incorrect ipspec %s' % ipSpec)


    def base_str_gen(self, base):
        x = 0
        while True:
            yield base + str(x)
            x += 1

    def sidspec_gen(self, base):
        if self.is_uuid_sid:
            while True:
                yield uuid.uuid4().int
        else:
            sid = base
            while True:
                yield sid
                sid += 1

    def send_remote(self, m, client):
        if 'sid' not in m:
            m['sid'] = client.cfg['sid'] if client is not None else 0
        self.web_client.send(m, cbResponse=client.receive_callback)

    def send_local(self, m, client=None):
        """ callback from a client """
        if 'sid' not in m:
            m['sid'] = client.cfg['sid'] if client is not None else 0
        self.received.append(m)
        r = {'sid': m['sid']}
        if client and 'receive_callback' in dir(client):
            self.rex.submit(lambda : client.receive_callback(r), delay=0)
            return
        return r

    def send(self, m, client=None):
        if self.web_client:
            return self.send_remote(m, client)
        return self.send_local(m, client)