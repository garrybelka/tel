from heapq import *
from twisted.internet import reactor


class RunExecutor(object):
    class Cancelled(object):
        pass
    CANCELLED = Cancelled()

    def __init__(self, twisted=False):
        self.rq = []    # initial request queue
        self.cur_time_ms = 0
        self.start_ms = reactor.seconds() * 1000

    def submit(self, req, delay=None, at=None):
        """ submit request to execute later, either at a given time or after delay
        """
        assert delay is not None or at is not None
        if delay is not None:
            t_ms = self.cur_time_ms + delay
        else:
            t_ms = at
        assert t_ms >= self.cur_time_ms
        entry = [t_ms, req]
        heappush(self.rq, entry)
        return entry

    def cancel(self, entry):
        if entry:
            entry[1] = self.CANCELLED

    def next(self):
        """ @return next [timestamp, request]
            @exception IndexError if no requests
        """
        while True:
            entry = heappop(self.rq)
            if entry[1] != self.CANCELLED:
                return entry

    def run(self):
        while True:
            try:
                t_ms, f = self.next()
            except IndexError:
                return
            self.cur_time_ms = t_ms
            f()

    def run_on_twisted(self):
        print 'run_on_twisted'
        reactor.callWhenRunning(lambda : self.twisted_cb())

    def twisted_cb(self):
        cur_t = reactor.seconds() * 1000 - self.start_ms
        self.cur_time_ms = cur_t
        while True:
            try:
                t_ms, f = self.rq[0]
                if t_ms <= cur_t:
                    heappop(self.rq)
                    if f != self.CANCELLED:
                        f()
                else:
                    reactor.callLater((t_ms - cur_t + 999) / 1000, self.twisted_cb)
                    return
            except IndexError:
                reactor.callLater(1, reactor.stop)
                return

