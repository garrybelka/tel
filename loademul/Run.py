import sys
from twisted.internet import reactor
from loademul import *
from util import Config


def main():
    config = Config(sys.argv[1], {})
    cfg = config.cfg
    twist = any('remote' in c for c in cfg)
    if twist:
        main_twisted(cfg)
    else:
        main_local(cfg)

def main_twisted(cfg):
    rex = RunExecutor(twisted=True)
    def done_handler():
        reactor.stop()
    lg = LoadGenerator(rex, cfg, done_handler)

    lg.run()
    rex.run_on_twisted()
    reactor.run()

def main_local(cfg):
    rex = RunExecutor()
    def done_handler():
        pass
    lg = LoadGenerator(rex, cfg, done_handler)

    lg.run()
    rex.run()

main()