import logging
from StringIO import StringIO
from twisted.internet import reactor
from twisted.web.client import Agent, FileBodyProducer
from twisted.web.http_headers import Headers
import json


LOG = logging.getLogger(__name__)


class WebClient(object):

    def __init__(self, url):
        self.url = url
        self.agent = Agent(reactor)

    def send(self, m, cbResponse=None, cbShutdown=None):
        j = json.dumps(m)
        d = self.agent.request(
            'POST',
            self.url,
            Headers({'User-Agent': ['Tel Client'],
                     'Content-Type': ['application/json']}),
            FileBodyProducer(StringIO(j)))

        if cbResponse is None:
            cbResponse = lambda r : None
        d.addCallback(cbResponse)

        if cbShutdown is None:
            cbShutdown = lambda r : None
        d.addBoth(cbShutdown)
