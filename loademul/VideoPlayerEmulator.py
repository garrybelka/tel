from collections import Counter
import sys


class Event:
    PLAY = 'PLAY'
    PAUSE = 'PAUSE'
    STOP = 'STOP'
    SEEK = 'SEEK'
    PLAYING = 'PLAYING'
    PLAYING_OFF = 'PLAYING_OFF'
    LOADING = 'LOADING'
    LOADING_OFF = 'LOADING_OFF'
    SET_BITRATE = 'SET_BITRATE'
    SET_BANDWIDTH = 'SET_BANDWIDTH'


class Error:
    BAD_SESSION_ID = 'bad or no session id'


class Setting:
    # start play if has at least this much of play time in buffer
    PLAY_START_BUFFER_MS = 2000

    # total buffer size
    TOTAL_BUFFER_MS = 20000

    # start loading if remains less than this much of play in buffer
    START_LOADING_MS = 5000

    # hb interval - how often to send hb msgs
    HB_INTERVAL_MS = 15000


DEFAULT_CFG = {
    'bandwidth': 1000000,
    'bitrate': 1000000,
    'request_play_at_start': False,
    'start_ms': 0,
    'end_ms': sys.maxint,
    'loss_ratio': 0.0,
}


class VideoPlayerEmulator(object):
    """ usage: vpe = VideoPlayerEmulator(rex, cfg, reporter, run=False)
    """
    def __init__(self, rex, cfg, reporter, run=False, done_cb=None):
        self.rex = rex
        self.cfg = cfg
        self.reporter = reporter
        self.done_cb = done_cb

        self.bandwidth = float(self.get_cfg('bandwidth'))
        self.bitrate = float(self.get_cfg('bitrate'))
        self.start_ms = self.get_cfg('start_ms')
        self.end_ms = self.get_cfg('end_ms')
        self.loss_ratio = self.get_cfg('loss_ratio')

        self.is_loading = False
        self.is_playing = False

        self.is_play_requested = self.get_cfg('request_play_at_start')
        self.is_stopped = False

        self.sid = None
        self.total_loaded_ms = 0
        self.total_loaded = 0.
        self.total_played_ms = 0
        self.total_played = 0.
        self.position_ms = 0.
        self.in_buffer = 0.
        self.last_sync_ms = 0
        self.task = None

        self.event_list = []
        self.errors = Counter()
        self.cur_errors = Counter()
        self.next_hb_ms = self.start_ms
        self.finished = False
        self.incoming_events = []
        self.msg_id = 0

        if run:
            self.run()

    def run(self):
        self.rex.submit(self.run_next, at=self.start_ms)

    def get_cfg(self, v):
        return self.cfg.get(v, DEFAULT_CFG[v])

    def run_next(self, cancel=False):
        if self.finished:
            return
        if cancel and self.task:
            self.rex.cancel(self.task)
            self.task = None
        states = self.is_loading, self.is_playing
        self.sync()
        self.last_sync_ms = self.rex.cur_time_ms
        if self.incoming_events:
            for e, p in self.incoming_events:
                self.do_event(e, p)
            self.incoming_events = []
            self.sync()
        if states != (self.is_loading, self.is_playing):
            self.report_state_changes(states)
        # are we done?
        if self.rex.cur_time_ms >= self.end_ms or self.is_stopped:
            self.finished = True
            self.rex.cancel(self.task)
            self.task = None
            self.send_hb()
            if self.done_cb:
                self.done_cb()
            return

        if self.next_hb_ms <= self.rex.cur_time_ms:
            self.send_hb()
        # schedule next run
        next_ms = min(self.next_hb_ms, self.end_ms)
        delta_ms = self.get_delta_to_next()
        next_ms = min(delta_ms + self.rex.cur_time_ms, next_ms) if delta_ms is not None else next_ms
        self.task = self.rex.submit(self.run_next, at=next_ms)

    def sync(self):
        t = self.rex.cur_time_ms - self.last_sync_ms
        self.last_sync_ms = self.rex.cur_time_ms
        if self.is_playing:
            self.total_played_ms += t
            self.position_ms += t
            b = t * self.bitrate / 8000.
            self.total_played += b
            self.in_buffer -= b
        if self.is_loading:
            self.total_loaded_ms += t
            b = t * self.bandwidth / 8000.
            self.total_loaded += b
            self.in_buffer += b

        if self.in_buffer < 0:
            self.in_buffer = 0
        else:
            lim = Setting.TOTAL_BUFFER_MS * self.bitrate / 8000.
            if self.in_buffer > lim:
                self.in_buffer = lim

        bw = self.bandwidth if self.is_loading else 0
        bw -= self.bitrate if self.is_playing else 0

        if not self.is_play_requested:
            self.is_playing = False

        if bw > 0:
            if not self.is_playing and self.is_play_requested:
                if self.in_buffer >= self.bitrate * Setting.PLAY_START_BUFFER_MS / 8000.:
                    self.is_playing = True
            if self.is_loading and self.in_buffer >= self.bitrate * Setting.TOTAL_BUFFER_MS / 8000.:
                self.is_loading = False
            return
        if bw < 0:
            if not self.is_loading and self.in_buffer <= self.bitrate * Setting.START_LOADING_MS / 8000.:
                self.is_loading = True
            if not self.is_play_requested or self.is_playing and self.in_buffer <= 0:
                self.is_playing = False
            return

        if self.is_playing:
            if self.in_buffer >= self.bitrate * Setting.TOTAL_BUFFER_MS / 8000.:
                self.is_loading = False
            return

        if self.is_play_requested and self.in_buffer >= self.bitrate * Setting.PLAY_START_BUFFER_MS / 8000.:
            self.is_playing = True

        if self.in_buffer >= self.bitrate * Setting.TOTAL_BUFFER_MS / 8000.:
            self.is_loading = False
        elif self.in_buffer <= self.bitrate * Setting.START_LOADING_MS / 8000.:
            self.is_loading = True

    def get_delta_to_next(self):
        """ @return time in ms until next expected event or None """
        bw = self.bandwidth if self.is_loading else 0
        bw -= self.bitrate if self.is_playing else 0
        t = self.in_buffer * 8000. / self.bitrate
        if bw > 0:
            if not self.is_playing and self.is_play_requested:
                td = (Setting.PLAY_START_BUFFER_MS - t) * self.bitrate / bw
                return td if td > 0 else 0
            td = (Setting.TOTAL_BUFFER_MS - t) * self.bitrate / bw
            return td if td > 0 else 0
        if bw < 0:
            bw = -bw
            td = (t - Setting.START_LOADING_MS) * self.bitrate / bw
            if not self.is_loading:
                return td if td > 0 else 0
            return t * self.bitrate / bw
        return None

    def report_state_changes(self, old_states):
        if self.is_stopped:
            return
        old_loading, old_playing = old_states
        if old_loading != self.is_loading:
            self.report_event(Event.LOADING if self.is_loading else Event.LOADING_OFF)
        if old_playing != self.is_playing:
            self.report_event(Event.PLAYING if self.is_playing else Event.PLAYING_OFF)

    def report_event(self, e):
        r = {'event': e,
             'ts': int(self.rex.cur_time_ms),
             'total_loaded': int(self.total_loaded),
             'total_played': int(self.total_played),
             'position_ms': int(self.position_ms),
             }
        self.event_list.append(r)

    def when_next_hb(self):
        """ when to send next hb after current time
            @return time in ms, aligned at HB_INTERVAL_MS w.r.t. start_ms """
        n = (self.rex.cur_time_ms - self.start_ms) / Setting.HB_INTERVAL_MS
        return self.start_ms + (n + 1) * Setting.HB_INTERVAL_MS

    def send_hb(self):
        self.msg_id += 1
        m = {
            'msgid': self.msg_id,
            'ts': self.rex.cur_time_ms,
            'total_loaded': int(self.total_loaded),
            'total_played': int(self.total_played),
            'position_ms': int(self.position_ms),
            'total_loaded_ms': int(self.total_loaded_ms),
            'total_played_ms': int(self.total_played_ms),
            'is_playing': self.is_playing,
            'is_play_requested': self.is_play_requested,
            'is_loading': self.is_loading,
            'events': self.event_list,
            'errors': self.errors,
            'cur_errors': self.cur_errors,
        }
        if self.sid is not None:
            m['sid'] = self.sid
        self.event_list = []
        self.cur_errors = Counter()
        self.next_hb_ms = self.when_next_hb()
        if self.next_hb_ms > self.end_ms > self.rex.cur_time_ms:
            self.next_hb_ms = self.end_ms
        if self.loss_ratio > 0:
            pass # TODO
        self.reporter.send(m, self)

    def receive_callback(self, r):
        if self.sid is None:
            self.sid = r.get('sid', None)
        elif self.sid != r.get('sid', None):
            self.mark_error(Error.BAD_SESSION_ID)

    def simulate_event(self, event, param=None):
        """ external request to emulate event
        """
        self.incoming_events.append((event, param))
        self.run_next(cancel=True)

    def do_event(self, event, param):
        if event == Event.PLAY:
            self.is_play_requested = True
        elif event == Event.PAUSE:
            self.is_play_requested = False
        elif event == Event.STOP:
            self.is_play_requested = False
            self.is_stopped = True
        elif event == Event.SEEK:
            self.position_ms = param
            self.in_buffer = 0
        elif event == Event.SET_BITRATE:
            self.bitrate = param
        elif event == Event.SET_BANDWIDTH:
            self.bandwidth = param
        self.report_event(event)

    def mark_error(self, errCode):
        self.cur_errors[errCode] = self.cur_errors[errCode] + 1
        self.errors[errCode] = self.errors[errCode] + 1

    def simulate_disconnect(self, duration):
        pass    # TODO



