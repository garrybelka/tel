from cassandra.cluster import Cluster
import logging


LOG = logging.getLogger(__name__)


class Cassy(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.cluster = Cluster(self.cfg['cassandra.contact_points'])
        self.session = self.cluster.connect(self.cfg['cassandra.keyspace'])

    def